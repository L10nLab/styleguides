# Summary

* [Introduction](README.md)

## Style Guides

* [Guidelines](guidelines/README.md)
    * [Style guide outline](guidelines/outline.md)
    * [Example style guide](guidelines/example_en-us.md)
* [Mozilla general style guide](mozilla_general/README.md)
* [Arabic (ar)](ar/README.md)
* [Bambara (bm)](bm/README.md)
    * [General Style Considerations](bm/general.md)
    * [Abbreviations, Acronyms, Articles, and Loan Words](bm/aaa.md)
    * [Non-translated and Loan Words](bm/loanwords.md)
    * [Word-level Guidelines](bm/wl.md)
    * [Sentence-level Guidelines](bm/sl.md)
    * [Appendix](bm/appendix.md)
* [French (fr)](fr/README.md)
* [Hindi (hi-IN)](hi-IN/README.md)
* [Portuguese (pt-BR)](pt-BR/README.md)
    * [General Style Guide](pt-BR/general.md)
    * [Glossary](pt-BR/glossary.md)
